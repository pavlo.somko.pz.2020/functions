const getSum = (str1, str2) => {  
  if(typeof str1 != "string" || typeof str2 != "string"){
    return false;
  }
  if(Number.isNaN(Number(str1))&& Number.isNaN(Number(str2))){
    return false;
  }
  return (Number(str1) + Number(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0, comments = 0;
  for(var post of listOfPosts){
      if(post.author == authorName){
        posts++;
      }
      if(post.hasOwnProperty("comments")){
        for (var comment of post.comments){
          if(comment.author == authorName){
            comments++;
          }
        }
      }
  }
  return 'Post:' + posts + ',comments:'+comments;
};

const tickets=(people)=> {
  var arr = [0];
  if(people[0]> 25){
    return "NO";
  }
  for (var i = 0; i < people.length-1; i++){
    var dif = people[i+1] - people[i];
    if(dif%25!=0 ){
      return "NO";
    }
    if(dif > 0){
      if(dif == 25){
        arr.push(people[i+1]);
      }
      if(dif == 0){
        if(people[i] == 25){
          arr.push(people[i]);
          arr.sort((x,y) => y-x);
        }
        dif = people[i+1];
          for(var elem of arr){
            dif = dif-elem;
            if(dif == 25){
              break;
            }
            if(dif < 25){
              return "NO";
            }
        }
        arr.push(people[i+1]);
        arr.sort((x,y) => y-x);        
      }
      if(dif > 25){
        for(var k of arr){
            dif = dif-k;
            if(dif == 25){
              break;
            }
            if(dif < 25){
              return "NO";
            }
        }
        if(dif > 25){
          return "NO";
        }
      }
    }  
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
